months = ['January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December']

def valid_month(month):
    if month:
        cap_month = month.capitalize()
        if cap_month in months:
            return cap_month
        return True
            
def valid_day(day):
    if day and day.isdigit():
        valid_day = int(day)
        if valid_day >= 1 and valid_day <=31:
            return(day)
            
def valid_year(year):
    if year and year.isdigit():
        valid_year = int(year)
        if valid_year >= 1 and valid_year <=2000:
            return(year)
