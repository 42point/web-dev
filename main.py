import webapp2
# import helper_validation

form = """
<form method="post">
    <h1>What is your birthday</h1>
    <p><label>Month<input type="text" name="month"></label></p>
    <p><label>Day<input type="text" name="day"></label></p>
    <p><label>Year<input type="text" name="year"></label></p>
    <p><input type="submit"></input></p>
</form>"""

months = ['January',
          'February',
          'March',
          'April',
          'May',
          'June',
          'July',
          'August',
          'September',
          'October',
          'November',
          'December']

def valid_month(month):
    if month:
        cap_month = month.capitalize()
        if cap_month in months:
            return cap_month
            
def valid_day(day):
    if day and day.isdigit():
        day = int(day)
        if day >= 1 and day <=31:
            return day
            
def valid_year(year):
    if year and year.isdigit():
        year = int(year)
        if year >= 1 and year <=2000:
            return year


class MainPage(webapp2.RequestHandler):
    def get(self):
#         self.response.headers['Content-Type'] = 'text/html'
        self.response.write(form)
    def post(self):
        user_month = valid_month(self.request.get('month'))
        user_day = valid_day(self.request.get('day'))
        user_year = valid_year(self.request.get('year'))
#         self.response.out.write("Thats correct!")
        if not user_month and user_day and user_year:
            self.response.out.write("Thats correct!")
        else:
            self.response.write(form)
            self.response.out.write("Something wrong")






# class TestHadler(webapp2.RequestHandler):
#     def post(self):
#         q = self.request.get("q")
#         self.response.out.write(q)
#         
#         self.response.headers['Content-Type'] = 'text/plain'
#         self.response.out.write(self.request)


app = webapp2.WSGIApplication([
    ('/', MainPage),
], debug=True)
